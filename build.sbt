organization := "com.gocardless"
name := "Domain Crawler"
version := "1.0-SNAPSHOT"

scalaVersion := "2.12.3"

resolvers += Classpaths.typesafeReleases

libraryDependencies ++= Seq(
  "org.json4s"  %% "json4s-native" % "3.5.0",
  "org.apache.httpcomponents" % "httpclient" % "4.5.1",
  "net.ruippeixotog" %% "scala-scraper" % "2.0.0",


  "org.scalatest" %% "scalatest" % "3.0.1" % "test",
  "com.squareup.okhttp3" % "mockwebserver" % "3.9.0" % "test"

)
