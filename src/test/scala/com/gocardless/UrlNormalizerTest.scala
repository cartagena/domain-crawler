package com.gocardless

import java.net.URL

import org.scalatest.{FlatSpec, Matchers}

class UrlNormalizerTest extends FlatSpec with Matchers {

  private val url = new URL("http://www.test.com")

  "Url Normalized" should "ignore other domains" in {
    val links = List(
      "http://www.google.com",
      "/test",
      "https://www.test.com/same"
    )

    val normalized = UrlNormalizer(url).toFullUrlOnSameDomainAndUnique(links)

    normalized should have size (2)
    normalized should not contain ("www.nodomain.com")
    every(normalized) should include("www.test.com")
  }

  it should "ignore fragment only" in {
    val links = List(
      "/test",
      "#bookmark"
    )

    val normalized = UrlNormalizer(url).toFullUrlOnSameDomainAndUnique(links)

    normalized should have size (1)
    normalized should not contain ("#bookark")
    every(normalized) should include("www.test.com")
  }

  it should "ignore duplicated links" in {
    val links = List(
      "/test",
      "/test"
    )

    val normalized = UrlNormalizer(url).toFullUrlOnSameDomainAndUnique(links)

    normalized should have size (1)
    every(normalized) should include("www.test.com")
  }

  it should "ignore duplicated links as fragments" in {
    val links = List(
      "/test#link1",
      "/test#link2"
    )

    val normalized = UrlNormalizer(url).toFullUrlOnSameDomainAndUnique(links)

    normalized should have size (1)
    every(normalized) should include("www.test.com")
  }

  it should "consider both https protocol" in {
    val links = List("https://www.test.com/secure")

    val normalized = UrlNormalizer(url).toFullUrlOnSameDomainAndUnique(links)

    normalized should have size (1)
    every(normalized) should include("www.test.com")
  }

  it should "ignore other protocols (http[s] only)" in {
    val links = List("ftp://www.test.com/download")

    val normalized = UrlNormalizer(url).toFullUrlOnSameDomainAndUnique(links)

    normalized shouldBe empty
  }

}
