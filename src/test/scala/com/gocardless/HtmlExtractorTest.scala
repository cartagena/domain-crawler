package com.gocardless

import org.scalatest.{FlatSpec, Matchers}

class HtmlExtractorTest extends FlatSpec with Matchers {

  "Html Extract" should "extract all links" in {
    val is = getClass.getResourceAsStream("/extractor/with-multi-links.html")
    val links = HtmlExtractor(is).extractLinks

    links should have size (9)
  }

  it should "extract images" in {
    val is = getClass.getResourceAsStream("/extractor/with-images-only.html")
    val assets = HtmlExtractor(is).extractAssets

    assets should have size (2)
    assets should contain only(s"logo.png", "http://my.cdn.com/bg.png")
  }

  it should "extract scripts" in {
    val is = getClass.getResourceAsStream("/extractor/with-scripts-only.html")
    val assets = HtmlExtractor(is).extractAssets

    assets should have size (2)
    assets should contain only(s"jquery.js", "http://my.cdn.com/vue.js")
  }

  it should "extract stylesheets" in {
    val is = getClass.getResourceAsStream("/extractor/with-stylesheets-only.html")
    val assets = HtmlExtractor(is).extractAssets

    assets should have size (2)
    assets should contain only(s"all.css", "http://my.cdn.com/ext.css")
  }

  it should "extract all assets" in {
    val is = getClass.getResourceAsStream("/extractor/with-all-assets.html")
    val assets = HtmlExtractor(is).extractAssets

    assets should have size (6)
    assets should contain only(
      s"all.css",
      s"jquery.js",
      s"logo.png",
      "http://my.cdn.com/ext.css",
      "http://my.cdn.com/bg.png",
      "http://my.cdn.com/vue.js"
    )
  }
}
