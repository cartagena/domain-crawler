package com.gocardless

import scala.io.Source

import org.scalatest.Inspectors._
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}

import okhttp3.mockwebserver._

class CrawlerTest extends FlatSpec with Matchers with BeforeAndAfter {

  private val indexResponse = buildResponse("crawler/index.html")
  private val test1Response = buildResponse("crawler/test1.html")
  private val test2Response = buildResponse("crawler/test2.html")
  private val irregularResponse = buildResponse("crawler/irregular.html")

  var mockServer = new MockWebServer

  before {
    mockServer = new MockWebServer
    mockServer.start
  }

  after {
    mockServer.shutdown
  }

  "Crawler" should "navigate to all links" in {
    mockServer.enqueue(indexResponse)
    mockServer.enqueue(test1Response)
    mockServer.enqueue(test2Response)

    val url = s"http://${mockServer.getHostName}:${mockServer.getPort}"
    val (visits, _) = Crawler(url).crawl

    visits should have size (3)
    forAll(visits) {
      _.url should include(url)
    }

    mockServer.getRequestCount should be(3)
  }

  it should "follow redirects" in {
    mockServer.enqueue(new MockResponse().setResponseCode(301).setHeader("Location", "crawler/test2.html"))
    mockServer.enqueue(test2Response)

    val url = s"http://${mockServer.getHostName}:${mockServer.getPort}"

    val (visits, _) = Crawler(url).crawl

    visits should have size (1)
    forAll(visits) {
      _.url should include(url)
    }

    mockServer.getRequestCount should be(2)
  }

  it should "handle not found pages" in {
    mockServer.enqueue(new MockResponse().setResponseCode(404))

    val url = s"http://${mockServer.getHostName}:${mockServer.getPort}"

    val (visits, _) = Crawler(url).crawl

    visits should have size (0)
    mockServer.getRequestCount should be(1)
  }

  it should "process only html documents" in {
    mockServer.enqueue(new MockResponse().addHeader("Content-Type", "application/json"))

    val url = s"http://${mockServer.getHostName}:${mockServer.getPort}"

    val (visits, _) = Crawler(url).crawl

    visits should have size (0)
    mockServer.getRequestCount should be(1)
  }

  it should "ignore phone, mail and javascript links" in {
    mockServer.enqueue(irregularResponse)

    val url = s"http://${mockServer.getHostName}:${mockServer.getPort}"

    val (visits, _) = Crawler(url).crawl

    visits should have size (1)
    mockServer.getRequestCount should be(1)
  }

  private def buildResponse(file: String) = {
    new MockResponse()
      .addHeader("Content-Type", "text/html")
      .setBody(Source.fromResource(file).mkString)
  }
}
