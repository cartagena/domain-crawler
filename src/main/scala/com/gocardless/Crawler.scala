package com.gocardless

import java.io.InputStream
import java.net.URL

import scala.annotation.tailrec
import scala.collection.mutable
import scala.util.{Failure, Success, Try}

import org.apache.http.client.config.RequestConfig
import org.apache.http.client.methods.{CloseableHttpResponse, HttpGet}
import org.apache.http.impl.client.HttpClientBuilder

case class Crawler(domainUrl: String) {

  private lazy val url = new URL(domainUrl)
  private lazy val normalizer = UrlNormalizer(url)
  private val visitQueue = mutable.Queue[String]()
  private val visitedLinks = mutable.Set[String]()

  def crawl = {
    visitQueue += domainUrl
    visitEnqueued(List(), List())
  }

  @tailrec
  private def visitEnqueued(visited: List[SiteReport], errors: List[String]): (List[SiteReport], List[String]) = if (visitQueue.isEmpty) {
    (visited, errors)
  } else {
    val currentLink = visitQueue.dequeue

    println(s"Visiting [$currentLink] ...")
    visitedLinks += currentLink

    Downloader.tryDownloadContent(currentLink) match {
      case Success(content) => {
        val report = buildReportAndQueue(visited, errors, currentLink, content)
        visitEnqueued(report :: visited, errors)
      }
      case Failure(e) => {
        println(s" ERROR! Queue size is: ${visitQueue.length} | Visited: ${visitedLinks.size} | Error: ${e.getMessage}")
        visitEnqueued(visited, currentLink :: errors)
      }
    }
  }

  private def buildReportAndQueue(visited: List[SiteReport], errors: List[String], currentLink: String, content: InputStream) = {
    val htmlExtractor = HtmlExtractor(content)

    val normalizedAssets = normalizer.toFullUrl(htmlExtractor.extractAssets)
    val normalizedLinks = normalizer.toFullUrlOnSameDomainAndUnique(htmlExtractor.extractLinks)

    visitQueue ++= filterLinks(normalizedLinks)
    println(s" DONE! Queue size is: ${visitQueue.length} | Visited: ${visitedLinks.size}")

    SiteReport(currentLink, normalizedAssets)
  }

  private def filterLinks(normalizedLinks: Seq[String]) = normalizedLinks filterNot {
    _.matches("(.*)(tel:|javascript:|mailto:)(.*)")
  } filterNot {
    visitedLinks.contains
  } filterNot {
    visitQueue.contains
  }
}

object Downloader {

  private val timeout = 2000
  private val config = RequestConfig.custom
    .setConnectTimeout(timeout)
    .setConnectionRequestTimeout(timeout)
    .setSocketTimeout(timeout)
    .build

  private lazy val client = HttpClientBuilder.create
    .setDefaultRequestConfig(config)
    .build

  def tryDownloadContent(link: String) = {
    val httpGet = new HttpGet(link)
    Try {
      val response = client.execute(httpGet)

      if (isErrorResponse(response)) {
        throw new IllegalStateException(s"Cannot handle request, got status ${response.getStatusLine.getStatusCode}.")
      }

      if(isSuccessResponse(response) && !isHtmlDocument(response)) {
        throw new IllegalStateException(s"Cannot handle request, content-type [${response.getEntity.getContentType.getValue}] is not [text/html].")
      }

      response.getEntity.getContent
    } recoverWith {
      case e: Exception => {
        httpGet.releaseConnection()
        Failure(e)
      }
    }
  }

  private def isErrorResponse(response: CloseableHttpResponse) = response.getStatusLine.getStatusCode >= 400

  private def isSuccessResponse(response: CloseableHttpResponse) = {
    response.getStatusLine.getStatusCode >= 200 && response.getStatusLine.getStatusCode < 300
  }

  private def isHtmlDocument(response: CloseableHttpResponse) = response.getEntity.getContentType.getValue.contains("text/html")

}

case class SiteReport(url: String, assets: Seq[String])
