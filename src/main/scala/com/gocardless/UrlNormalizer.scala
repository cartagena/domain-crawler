package com.gocardless

import java.net.URL

import scala.util.{Failure, Success, Try}

case class UrlNormalizer(baseUrl: URL) {

  private lazy val domain = baseUrl.toString.replace(baseUrl.getPath, "")

  def toFullUrlOnSameDomainAndUnique(urls: Seq[String]) = toFullUrl(urls) filter { url =>
    isHypertextProtocol(url) && isSameDomain(url)
  } distinct

  def toFullUrl(urls: Seq[String]) = (urls map normalize)

  private def normalize(url: String) = Try(new URL(url)) match {
    case Success(_) => url
    case Failure(_) => if (!url.startsWith("#")) buildFullUrl(url) else url
  }

  private def buildFullUrl(url: String) = {
    val path = if (url.startsWith("/")) url else s"/$url"
    removeFragment(s"$domain$path")
  }

  private def isHypertextProtocol(link: String) = link.matches("https?.*")

  private def isSameDomain(link: String) = new URL(link).getHost.equals(baseUrl.getHost)

  private def removeFragment(link: String) = link.takeWhile(!_.equals('#'))

}
