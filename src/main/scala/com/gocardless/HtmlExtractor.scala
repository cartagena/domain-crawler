package com.gocardless

import java.io.InputStream

import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL._

case class HtmlExtractor(html: InputStream) {

  private lazy val doc = JsoupBrowser().parseInputStream(html)

  def extractAssets: Seq[String] = {
    val images = (doc >> elementList("img")) filter (_.hasAttr("src")) map (_.attr("src"))
    val scripts = (doc >> elementList("script")) filter (_.hasAttr("src")) map (_.attr("src"))
    val stylesheets = (doc >> elementList("link[rel='stylesheet']")) filter (_.hasAttr("href")) map (_.attr("href"))

    (images ++ scripts ++ stylesheets)
  }

  def extractLinks: Seq[String] = (doc >> elementList("a")) filter (_.hasAttr("href")) map (_.attr("href"))

}
