package com.gocardless

import org.json4s.DefaultFormats
import org.json4s.native.Serialization.writePretty

object CrawlerApp extends App {

  implicit val jsonFormats = DefaultFormats

  val usage =
    """
    Usage: crawler -d domain [--show-errors]
  """

  if (args.length == 0) println(usage)

  def parseOptions(map: Map[String, String], list: List[String]): Map[String, String] = {
    list match {
      case Nil => map
      case "-d" :: value :: tail =>
        parseOptions(map ++ Map("domain" -> value), tail)
      case "--show-errors" :: tail =>
        parseOptions(map ++ Map("errors" -> "true"), tail)
    }
  }

  val options = parseOptions(Map(), args.toList)
  val domain = options("domain")
  val showErrors = options.contains("errors")

  val (results, errors) = Crawler(domain).crawl
  println(writePretty(results))

  if (showErrors) println(writePretty(("errors" -> errors)))
}
