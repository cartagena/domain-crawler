# Domain crawler
This simple domain crawler will crawl a website and return a list of static assets (images, javascript and stylesheets) founds on each page.

# Install

- Install Java 8
  ```
  brew cask install java
  ```


- Install Java 8
  ```
  brew install sbt
  ```

# Test

- To run all test you should use:
  ```
  sbt test
  ```

# Run

- Running is also simple. Just replace the `<url>` by the inital URL you want to crawl. If using `show-errors` a list of unreachable links will be print at the end.
  ```
  sbt "run-main com.gocardless.CrawlerApp -d <url> [--show-errors]"
  ```
